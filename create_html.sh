#!/bin/bash

dir="./public/article"

# articles=$(find ./public/article -type f -printf "%f\n")

touch ./public/article/tree.txt
echo > ./public/article/tree.txt
echo "$(date)" > ./public/article/tree.txt


for entry in "$dir"/*.adoc
do
  filename=$(basename -- "$entry")
  extension="${filename##*.}"
  filename="${filename%.*}"

  line=$(head -n 1 "$entry")
  title=$(sed '2q;d' "$entry")

  echo "$filename.$extension | $title | $line" >> ./public/article/tree.txt

  title="$(tr '[:lower:]' '[:upper:]' <<< "${filename:0:1}")${filename:1}"
  touch "./public/article/$filename.html"

  # HTML file
  {
    echo "<!DOCTYPE html>"
    echo "<html lang='fr'>"
    echo "  <head>"
    echo "    <meta charset='UTF-8' />"
    echo "    <meta name='viewport' content='width=device-width, initial-scale=1.0' />"
    echo "    <link rel='icon' type='image/png' href='../img/favicon.png' />"
    echo "    <link rel='stylesheet' href='../css/main.css' />"
    echo "    <link href='../font/fontawesome/css/all.min.css' rel='stylesheet' />"
    echo "    <title>$filename</title>"
    echo "  </head>"
    echo "  <body id='blog'>"
    echo "    <section id='main'></section>"

    echo "    <aside id='menu-section' class='hidden' aria-label='nav'>"
    echo "      <div class='header'>"
    echo "        <i class='fas fa-bars fa-2x'></i>"
    echo "        <h1>Menu</h1>"
    echo "      </div>"

    echo "      <div class='row' id='home-btn'>"
    echo "        <i class='fas fa-house fa-2x'></i>"
    echo "        <h2>Accueil</h2>"
    echo "      </div>"

    echo "      <div class='row' id='about-btn'>"
    echo "        <i class='fas fa-fingerprint fa-2x'></i>"
    echo "        <h2>À propos</h2>"
    echo "      </div>"

    echo "      <div class='row' id='search-btn'>"
    echo "        <i class='fas fa-magnifying-glass fa-2x'></i>"
    echo "        <h2>Recherche</h2>"
    echo "      </div>"

    echo "      <div class='row' id='categories-btn'>"
    echo "        <i class='fas fa-layer-group fa-2x'></i>"
    echo "        <h2>Catégories</h2>"
    echo "      </div>"

    echo "      <div class='row' id='tags-btn'>"
    echo "        <i class='fas fa-tags fa-2x'></i>"
    echo "        <h2>Tags</h2>"
    echo "      </div>"
    echo "    </aside>"

    echo "    <aside id='tags-section' class='hidden' aria-label='nav'>"
    echo "      <div class='header'>"
    echo "        <i class='fas fa-tags fa-2x'></i>"
    echo "        <h1>Tags</h1>"
    echo "      </div>"
    echo "      <section id='tags'></section>"
    echo "    </aside>"

    echo "    <aside id='categories-section' class='hidden' aria-label='nav'>"
    echo "      <div class='header'>"
    echo "        <i class='fas fa-layer-group fa-2x'></i>"
    echo "        <h1>Catégories</h1>"
    echo "      </div>"
    echo "      <section id='categories'></section>"
    echo "    </aside>"

    echo "    <aside id='search-section' class='hidden' aria-label='nav'>"
    echo "      <div class='header'>"
    echo "        <i class='fas fa-magnifying-glass fa-2x'></i>"
    echo "        <h1>Recherche</h1>"
    echo "      </div>"
    echo "      <input type='text' id='search-input' name='search'/>"
    echo "      <section id='search'></section>"
    echo "    </aside>"

    echo "    <aside id='summary-section' class='hidden' aria-label='nav'>"
    echo "      <div class='header'>"
    echo "        <i class='fas fa-align-left fa-2x'></i>"
    echo "        <h1>Sommaire</h1>"
    echo "      </div>"
    echo "      <section id='summary'></section>"
    echo "    </aside>"

    echo "    <aside id='dynamic-section' class='hidden' aria-label='nav'>"
    echo "      <div class='header'>"
    echo "        <i id='dynamic-icon'></i>"
    echo "        <h1 id='dynamic-title'></h1>"
    echo "      </div>"
    echo "      <section id='dynamic'></section>"
    echo "    </aside>"

    echo "    <nav>"
    echo "      <p class='btn' id='commento-btn'><i class='fas fa-comment fa-3x'></i></p>"
    echo "      <p class='btn' id='summary-btn'><i class='fas fa-align-left fa-3x'></i></p>"
    echo "      <p class='btn' id='menu-btn'><i class='fas fa-bars fa-3x'></i></p>"
    echo "      <p class='btn hidden' id='close-btn'><i class='fas fa-times fa-3x'></i></p>"
    echo "    </nav>"

    echo "    <div id='commento' class='hidden'></div>"

    echo "    <!-- Popup -->"
    echo "    <div id='popup-section'></div>"
    echo "    <script defer src='https://cdn.commento.io/js/commento.js'></script>"
    echo "    <script src='../js/main.js' type='module'></script>"
    echo "  </body>"
    echo "</html>"
  } > "./public/article/$filename.html"
done
