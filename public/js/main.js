'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
import { test } from "./class/parser.js";
test("youpi");
var allRegex = [
    { "id": 0, "value": "", "level": 0, "type": "h1", "rule": /^=\s(.+)/ },
    { "id": 0, "value": "", "level": 0, "type": "h2", "rule": /^==\s(.+)/ },
    { "id": 0, "value": "", "level": 0, "type": "h3", "rule": /^===\s(.+)/ },
    { "id": 0, "value": "", "level": 0, "type": "h4", "rule": /^====\s(.+)/ },
    { "id": 0, "value": "", "level": 0, "type": "h5", "rule": /^=====\s(.+)/ },
    { "id": 0, "value": "", "level": 0, "type": "h6", "rule": /^======\s(.+)/ },
    { "id": 0, "value": "", "level": 0, "type": "ul", "rule": /^\*\s(.+)/ },
    { "id": 0, "value": "", "level": 1, "type": "ul", "rule": /^\*\*\s(.+)/ },
    { "id": 0, "value": "", "level": 2, "type": "ul", "rule": /^\*\*\*\s(.+)/ },
    { "id": 0, "value": "", "level": 3, "type": "ul", "rule": /^\*\*\*\*\s(.+)/ },
    { "id": 0, "value": "", "level": 0, "type": "ol", "rule": /^\.\s(.+)/ },
    { "id": 0, "value": "", "level": 1, "type": "ol", "rule": /^\.\.\s(.+)/ },
    { "id": 0, "value": "", "level": 2, "type": "ol", "rule": /^\.\.\.\s(.+)/ },
    { "id": 0, "value": "", "level": 3, "type": "ol", "rule": /^\.\.\.\.\s(.+)/ },
    { "id": 0, "value": "", "level": 0, "type": "no", "rule": /^>\s(.+)/ },
    { "id": 0, "value": "", "level": 0, "type": "ce", "rule": /^```(end)/ },
    { "id": 0, "value": "", "level": 0, "type": "co", "rule": /^```(.+)/ }
];
var regexElt = [
    { "type": "italic_bold", "rule": /\*\*\*([^\*]+)\*\*\*/, "tag": "<strong>$1</strong>" },
    { "type": "bold", "rule": /\*\*([^\*]+)\*\*/, "tag": "<b>$1</b>" },
    { "type": "italic", "rule": /\*([^\*]+)\*/, "tag": "<i>$1</i>" },
    { "type": "link", "rule": /(\S+)\[(.+)\]/, "tag": "<a href='$1'>$2</a>" },
    { "type": "code", "rule": /`([^`]+)`/, "tag": "<mark>$1</mark>" }
];
var menuBtn = document.getElementById("menu-btn");
var closeBtn = document.getElementById("close-btn");
var aboutBtn = document.getElementById("about-btn");
var searchBtn = document.getElementById("search-btn");
var categoriesBtn = document.getElementById("categories-btn");
var tagsBtn = document.getElementById("tags-btn");
var menuSection = document.getElementById('menu-section');
var tagsSection = document.getElementById('tags-section');
var categoriesSection = document.getElementById('categories-section');
var searchSection = document.getElementById('search-section');
var dynamicSection = document.getElementById('dynamic-section');
var dynamicIcon = document.getElementById('dynamic-icon');
var dynamicTitle = document.getElementById('dynamic-title');
var dynamic = document.getElementById('dynamic');
var sections = new Map();
sections.set('menuBtn', menuBtn);
sections.set('closeBtn', closeBtn);
sections.set('menuSection', menuSection);
sections.set('tagsSection', tagsSection);
sections.set('categoriesSection', categoriesSection);
sections.set('searchSection', searchSection);
sections.set('dynamicSection', dynamicSection);
var isLocal = /(.+\/)[^\/]+\.html/.exec(window.location.href);
var urlPart = "";
if (isLocal !== null) {
    urlPart = getURLPart(/(.+\/)[^\/]+\.html/);
}
else {
    urlPart = window.location.href + "/";
}
var isArticle = /(\/article\/)/.exec(window.location.href) !== null;
var index = "";
if (!isArticle)
    index = "article/";
var windowLocation = "articles";
function elt(type, params) {
    var e_1, _a;
    var HTMLElt = document.createElement(type);
    if (params.content !== undefined)
        HTMLElt.textContent = params.content;
    if (params.id !== undefined)
        HTMLElt.setAttribute("id", params.id);
    if (params["class"] !== undefined)
        HTMLElt.className = params["class"];
    if (params.innerHTML !== undefined)
        HTMLElt.innerHTML = params.innerHTML;
    if (params.additions !== undefined && Array.isArray(params.additions)) {
        params.additions.forEach(function (element) {
            HTMLElt.appendChild(element);
        });
    }
    if (params.stringAttributes !== undefined) {
        try {
            for (var _b = __values(Object.entries(params.stringAttributes)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var _d = __read(_c.value, 2), key = _d[0], value = _d[1];
                HTMLElt.dataset[key] = value.toString();
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b["return"])) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    }
    return HTMLElt;
}
function icon(name, size, type) {
    if (size === void 0) { size = 1; }
    if (type === void 0) { type = "s"; }
    return elt("p", { "class": "icon", "additions": [
            elt("i", { "class": "fa".concat(type, " fa-").concat(name, " fa-").concat(size, "x") })
        ] });
}
function parse(text) {
    var summary = document.getElementById('summary');
    var lines = text.split('\n');
    var divResult = document.createElement("div");
    var result = getAllClassifiedLine(lines);
    if (result[0].type !== "h1") {
        result.shift();
    }
    for (var i = 0; i < result.length; i++) {
        if (result[i].type === "ul" || result[i].type === "ol") {
            var list = getAllList(result, i);
            divResult.appendChild(displayList(list.list));
            i = list.id;
        }
        else if (result[i].type === "co") {
            var code = getCodeBlock(result, i);
            divResult.appendChild(displayCode(code.list));
            i = code.id;
        }
        else if (result[i].type === "no") {
            divResult.appendChild(elt("div", { "class": "note", "additions": [
                    elt("p", { "innerHTML": parseLine(result[i].value) })
                ] }));
        }
        else if (result[i].type !== "split") {
            var element = elt(result[i].type, { "innerHTML": parseLine(result[i].value) });
            if (result[i].type.startsWith("h")) {
                var link = document.createElement("a");
                link.innerHTML = parseLine(result[i].value);
                link.href = "#".concat(i);
                link.addEventListener("click", function () {
                    displayOrHideElements(["menuBtn", "summaryBtn"], "articles");
                });
                summary.appendChild(link);
                element.id = i.toString();
            }
            divResult.appendChild(element);
        }
    }
    ;
    return divResult;
}
function getAllClassifiedLine(lines) {
    var e_2, _a, e_3, _b;
    var result = [];
    var listIteration = 0;
    try {
        for (var lines_1 = __values(lines), lines_1_1 = lines_1.next(); !lines_1_1.done; lines_1_1 = lines_1.next()) {
            var line = lines_1_1.value;
            var findRegex = false;
            try {
                for (var allRegex_1 = (e_3 = void 0, __values(allRegex)), allRegex_1_1 = allRegex_1.next(); !allRegex_1_1.done; allRegex_1_1 = allRegex_1.next()) {
                    var regex = allRegex_1_1.value;
                    var element = regex["rule"].exec(line);
                    if (element) {
                        findRegex = true;
                        if (regex.type === "ol" || regex.type === "ul") {
                            result.push({ "id": listIteration, "value": element[1], "level": regex.level, "type": regex.type, "rule": /a/ });
                            listIteration++;
                        }
                        else {
                            result.push({ "id": 0, "value": element[1], "level": 0, "type": regex.type, "rule": /a/ });
                            listIteration = 0;
                        }
                        break;
                    }
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (allRegex_1_1 && !allRegex_1_1.done && (_b = allRegex_1["return"])) _b.call(allRegex_1);
                }
                finally { if (e_3) throw e_3.error; }
            }
            if (!findRegex && line.trim().length !== 0) {
                result.push({ "id": 0, "value": line, "level": 0, "type": "p", "rule": /a/ });
                listIteration = 0;
            }
            else if (line.trim().length === 0) {
                result.push({ "id": 0, "value": "", "level": 0, "type": "split", "rule": /a/ });
                listIteration = 0;
            }
        }
    }
    catch (e_2_1) { e_2 = { error: e_2_1 }; }
    finally {
        try {
            if (lines_1_1 && !lines_1_1.done && (_a = lines_1["return"])) _a.call(lines_1);
        }
        finally { if (e_2) throw e_2.error; }
    }
    return result;
}
function getAllList(list, position) {
    var listElt = [];
    var i = position;
    for (i; i < list.length; i++) {
        if (list[i].type === "ul" || list[i].type === "ol") {
            listElt.push(list[i]);
        }
        else {
            break;
        }
    }
    return {
        "id": i,
        "list": listElt
    };
}
function displayList(list) {
    var listElt = elt(list[0].type, {});
    for (var i = 0; i < list.length; i++) {
        if (list[i].level === 0) {
            listElt.appendChild(displayListElt(list, i));
        }
    }
    return listElt;
}
function displayListElt(list, eltId) {
    var li = elt("li", { "innerHTML": parseLine(list[eltId].value) });
    var children = getChildren(list, eltId, list[eltId].level + 1);
    if (children.length > 0) {
        var subList_1 = elt(children[0].type, {});
        children.forEach(function (child) {
            subList_1.appendChild(displayListElt(list, child.id));
        });
        li.appendChild(subList_1);
    }
    return li;
}
function getChildren(list, previousId, childrenLevel) {
    var children = [];
    for (var i = previousId + 1; i < list.length; i++) {
        if (list[i].level <= list[previousId].level) {
            break;
        }
        if (list[i].level === childrenLevel) {
            children.push(list[i]);
        }
    }
    return children;
}
function getCodeBlock(list, position) {
    var listElt = [];
    var i = position;
    for (i; i < list.length; i++) {
        if (list[i].type === "ce") {
            break;
        }
        else {
            listElt.push(list[i]);
        }
    }
    return {
        "id": i + 1,
        "list": listElt
    };
}
function displayCode(list) {
    var listElt = elt("section", { "class": "code ".concat(list[0].value) });
    for (var i = 1; i < list.length; i++) {
        listElt.appendChild(elt("p", { "content": list[i].value }));
    }
    return listElt;
}
function parseLine(line) {
    regexElt.forEach(function (regex) {
        var ifRegex;
        while (ifRegex !== null) {
            ifRegex = regex["rule"].exec(line);
            if (ifRegex) {
                line = line.replace(regex.rule, regex.tag);
            }
            else {
                ifRegex = null;
            }
        }
    });
    return line;
}
function getURLContent(url, fct) {
    if (fct === void 0) { fct = null; }
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, fetch(url, {
                        method: "get",
                        headers: { "Content-Type": "application/json" }
                    })
                        .then(function (response) { return response.text(); })
                        .then(function (content) {
                        if (fct !== null)
                            fct(content);
                    })];
                case 1:
                    _a.sent();
                    return [2];
            }
        });
    });
}
function getURLPart(regex) {
    return regex.exec(window.location.href)[1];
}
function displayText(text) {
    document.getElementById("main").appendChild(parse(text));
}
function sortByDateDesc(array) {
    array.sort(function (a, b) {
        if (a.date < b.date) {
            return 1;
        }
        if (a.date > b.date) {
            return -1;
        }
        return 0;
    });
}
function displaySummary(file) {
    var id = /=(.+)/.exec(file)[1].trim().replace(" ", "_").toLowerCase() + "_" + windowLocation;
    var HTMLElement = document.getElementById(id);
    var summary = /```summary\n([^`]+)\n```end/.exec(file)[1];
    HTMLElement.appendChild(elt("p", { "content": summary }));
}
function displayArticle(article, location) {
    var link = document.createElement("a");
    link.href = "".concat(urlPart).concat(index).concat(article.url);
    link.id = article.name.trim().replace(" ", "_").toLowerCase() + "_" + location;
    link.className = "link";
    var title = document.createElement("p");
    var hr = document.createElement("hr");
    var tags = elt("div", { "class": "tags" });
    title.innerHTML = "<span>".concat(article.category, " :</span> ").concat(article.name);
    article.tags.forEach(function (tag) {
        tags.appendChild(elt("p", { "class": "chip", content: tag }));
    });
    link.appendChild(title);
    link.appendChild(hr);
    link.appendChild(tags);
    getURLContent("".concat(urlPart).concat(index).concat(article.url.replace(/.html/, '.adoc')), displaySummary);
    return link;
}
function displayTree(tree) {
    var e_4, _a;
    var _b, _c, _d, _e, _f;
    var articleResultArray = { tags: new Map([]), categories: new Map([]), articles: [] };
    var date = localStorage.getItem("date");
    if (date === null || date !== tree.split('\n').shift()) {
        localStorage.setItem("date", tree.split('\n').shift());
        var treeArray = tree.split('\n');
        for (var i = 1; i < treeArray.length; i++) {
            if (treeArray[i] !== "") {
                var url = "".concat(((_b = /(.+).adoc/.exec(treeArray[i])) !== null && _b !== void 0 ? _b : ["", "../index"])[1].trim(), ".html");
                var name_1 = (((_c = /=(.+)\|/.exec(treeArray[i])) !== null && _c !== void 0 ? _c : ["", "Aucun nom"])[1]).trim();
                var date_1 = (((_d = /\|([^|:]+):/.exec(treeArray[i])) !== null && _d !== void 0 ? _d : ["", "Aucune date"])[1]).trim();
                var type = (((_e = /:([^|:]+):/.exec(treeArray[i])) !== null && _e !== void 0 ? _e : ["", "Aucune catégorie"])[1]).trim();
                var tags = (((_f = /:(?:.+):([^:]+)/.exec(treeArray[i])) !== null && _f !== void 0 ? _f : ["", "Aucun"])[1]).replace(/\s/g, '').split(",");
                var link = { name: name_1, url: url, date: date_1, tags: tags, category: type };
                articleResultArray.articles.push(link);
                try {
                    for (var tags_1 = (e_4 = void 0, __values(tags)), tags_1_1 = tags_1.next(); !tags_1_1.done; tags_1_1 = tags_1.next()) {
                        var tag = tags_1_1.value;
                        var array = articleResultArray.tags.get(tag);
                        if (array === undefined) {
                            array = [];
                        }
                        array.push(link);
                        articleResultArray.tags.set(tag, array);
                    }
                }
                catch (e_4_1) { e_4 = { error: e_4_1 }; }
                finally {
                    try {
                        if (tags_1_1 && !tags_1_1.done && (_a = tags_1["return"])) _a.call(tags_1);
                    }
                    finally { if (e_4) throw e_4.error; }
                }
                var category = articleResultArray.categories.get(type);
                if (category === undefined) {
                    category = [];
                }
                category.push(link);
                articleResultArray.categories.set(type, category);
            }
        }
        articleResultArray.tags = new Map(__spreadArray([], __read(articleResultArray.tags.entries()), false).sort());
        articleResultArray.tags.forEach(function (tag, key) {
            sortByDateDesc(articleResultArray.tags.get(key));
            var chip = elt("div", {
                "class": "chip",
                "additions": [
                    elt("p", { "content": "".concat(tag.length), "class": "label" }),
                    elt("p", { "content": key, "class": "tag-".concat(tag.length) })
                ]
            });
            chip.addEventListener("click", function () {
                dynamicIcon.className = "fas fa-tags fa-2x";
                dynamicTitle.innerHTML = key;
                dynamic.innerHTML = "";
                articleResultArray.tags.get(key).forEach(function (article) {
                    dynamic.appendChild(displayArticle(article, "tags"));
                });
                displayOrHideElements(["menuBtn", "dynamicSection", "closeBtn"], "tags");
            });
            document.getElementById("tags").appendChild(chip);
        });
        articleResultArray.categories = new Map(__spreadArray([], __read(articleResultArray.categories.entries()), false).sort());
        articleResultArray.categories.forEach(function (category, key) {
            sortByDateDesc(articleResultArray.categories.get(key));
            var chip = elt("div", {
                "class": "chip",
                "additions": [
                    elt("p", { "content": "".concat(category.length), "class": "label" }),
                    elt("p", { "content": key, "class": "tag-".concat(category.length) })
                ]
            });
            chip.addEventListener("click", function () {
                dynamicIcon.className = "fas fa-layer-group fa-2x";
                dynamicTitle.innerHTML = key;
                dynamic.innerHTML = "";
                articleResultArray.categories.get(key).forEach(function (article) {
                    dynamic.appendChild(displayArticle(article, "categories"));
                });
                displayOrHideElements(["menuBtn", "dynamicSection", "closeBtn"], "categories");
            });
            document.getElementById("categories").appendChild(chip);
        });
        sortByDateDesc(articleResultArray.articles);
        if (!isArticle) {
            articleResultArray.articles.forEach(function (article) {
                var main = document.getElementById("main");
                main.appendChild(displayArticle(article, "articles"));
            });
        }
        localStorage.setItem("articles", JSON.stringify(articleResultArray.articles));
        localStorage.setItem("tags", JSON.stringify(articleResultArray.tags, replacer));
        localStorage.setItem("categories", JSON.stringify(articleResultArray.categories, replacer));
    }
    else {
        articleResultArray = JSON.parse(localStorage.getItem("tags"), reviver);
    }
}
function replacer(key, value) {
    if (value instanceof Map) {
        return {
            dataType: 'Map',
            value: Array.from(value.entries())
        };
    }
    else {
        return value;
    }
}
function reviver(key, value) {
    if (typeof value === 'object' && value !== null) {
        var test_1 = new Map();
        test_1.set(key, value);
        return test_1;
    }
    return value;
}
function displayOrHideElements(elementsToDisplay, location) {
    if (location === void 0) { location = ""; }
    windowLocation = location;
    sections.forEach(function (section, key) {
        if (elementsToDisplay.includes(key)) {
            section.style.display = "flex";
        }
        else {
            section.style.display = "none";
        }
    });
}
menuBtn.addEventListener("click", function () {
    displayOrHideElements(["menuSection", "closeBtn"], "menu");
});
closeBtn.addEventListener("click", function () {
    displayOrHideElements(["menuBtn", "summaryBtn", "commentoBtn"], "articles");
});
aboutBtn.addEventListener("click", function () {
    console.log("about");
});
searchBtn.addEventListener("click", function () {
    displayOrHideElements(["menuBtn", "closeBtn", "searchSection"], "search");
});
categoriesBtn.addEventListener("click", function () {
    displayOrHideElements(["menuBtn", "closeBtn", "categoriesSection"], "categories");
});
tagsBtn.addEventListener("click", function () {
    displayOrHideElements(["menuBtn", "closeBtn", "tagsSection"], "tags");
});
if (!isArticle) {
    getURLContent("".concat(urlPart, "article/tree.txt"), displayTree);
}
else {
    getURLContent("".concat(urlPart).concat(getURLPart(/\/([^\/]+).html/), ".adoc"), displayText);
    getURLContent("".concat(urlPart, "tree.txt"), displayTree);
    var homeBtn = document.getElementById("home-btn");
    homeBtn.addEventListener("click", function () {
        document.location.href = "../index.html";
    });
    var summaryBtn = document.getElementById("summary-btn");
    sections.set('summaryBtn', summaryBtn);
    var summarySection = document.getElementById('summary-section');
    sections.set('summarySection', summarySection);
    summaryBtn.addEventListener("click", function () {
        displayOrHideElements(["summarySection", "closeBtn", "menuBtn"], "summary");
    });
    var commentoBtn = document.getElementById('commento-btn');
    sections.set('commentoBtn', commentoBtn);
    var commentoSection = document.getElementById('commento');
    sections.set('commentoSection', commentoSection);
    commentoBtn.addEventListener("click", function () {
        displayOrHideElements(["commentoSection", "closeBtn"], "comment");
    });
}
//# sourceMappingURL=main.js.map