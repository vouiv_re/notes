'use strict';

import { test } from "./class/parser.js"

test("youpi")

///////////////
// Interface //
///////////////

interface Element {
    type: string,
    content: string,
    id: string,
    class: string,
    additions: Array<HTMLElement>,
    stringAttributes: string,
    innerHTML: string
}

interface Line {
    id: number,
    value: string,
    level: number,
    type: string,
    rule: RegExp,
}

interface CodeElt {
    type: string,
    rule: RegExp,
    tag: string
}

interface Tag {
    [key: string]: Array<Link>
}

interface Category {
    name: string,
    articles: Array<Link>
}

interface Link {
    name: string,
    url: string,
    date: string,
    tags: Array<string>,
    category: string
}

interface ResultArray {
    tags: Map<string, Array<Link>>,
    categories: Map<string, Array<Link>>,
    articles: Array<Link>
}

///////////
// Regex //
///////////

let allRegex: Array<Line>  = [
    {"id": 0, "value": "", "level": 0, "type": "h1", "rule": /^=\s(.+)/},
    {"id": 0, "value": "", "level": 0, "type": "h2", "rule": /^==\s(.+)/},
    {"id": 0, "value": "", "level": 0, "type": "h3", "rule": /^===\s(.+)/},
    {"id": 0, "value": "", "level": 0, "type": "h4", "rule": /^====\s(.+)/},
    {"id": 0, "value": "", "level": 0, "type": "h5", "rule": /^=====\s(.+)/},
    {"id": 0, "value": "", "level": 0, "type": "h6", "rule": /^======\s(.+)/},
    {"id": 0, "value": "", "level": 0, "type": "ul", "rule": /^\*\s(.+)/},
    {"id": 0, "value": "", "level": 1, "type": "ul", "rule": /^\*\*\s(.+)/},
    {"id": 0, "value": "", "level": 2, "type": "ul", "rule": /^\*\*\*\s(.+)/},
    {"id": 0, "value": "", "level": 3, "type": "ul", "rule": /^\*\*\*\*\s(.+)/},
    {"id": 0, "value": "", "level": 0, "type": "ol", "rule": /^\.\s(.+)/},
    {"id": 0, "value": "", "level": 1, "type": "ol", "rule": /^\.\.\s(.+)/},
    {"id": 0, "value": "", "level": 2, "type": "ol", "rule": /^\.\.\.\s(.+)/},
    {"id": 0, "value": "", "level": 3, "type": "ol", "rule": /^\.\.\.\.\s(.+)/},
    {"id": 0, "value": "", "level": 0, "type": "no", "rule": /^>\s(.+)/},
    {"id": 0, "value": "", "level": 0, "type": "ce", "rule": /^```(end)/},
    {"id": 0, "value": "", "level": 0, "type": "co", "rule": /^```(.+)/}
]

let regexElt: Array<CodeElt>  = [
    {"type": "italic_bold", "rule": /\*\*\*([^\*]+)\*\*\*/, "tag": "<strong>$1</strong>"},
    {"type": "bold", "rule": /\*\*([^\*]+)\*\*/, "tag": "<b>$1</b>"},
    {"type": "italic", "rule": /\*([^\*]+)\*/, "tag": "<i>$1</i>"},
    {"type": "link", "rule": /(\S+)\[(.+)\]/, "tag": "<a href='$1'>$2</a>"},
    {"type": "code", "rule": /`([^`]+)`/, "tag": "<mark>$1</mark>"}
]

////////////////
// Definition //
////////////////

let menuBtn = document.getElementById("menu-btn")
let closeBtn = document.getElementById("close-btn")
let aboutBtn = document.getElementById("about-btn")
let searchBtn = document.getElementById("search-btn")
let categoriesBtn = document.getElementById("categories-btn")
let tagsBtn = document.getElementById("tags-btn")

let menuSection = document.getElementById('menu-section')
let tagsSection = document.getElementById('tags-section')
let categoriesSection = document.getElementById('categories-section')
let searchSection = document.getElementById('search-section')
let dynamicSection = document.getElementById('dynamic-section')

let dynamicIcon = document.getElementById('dynamic-icon')
let dynamicTitle = document.getElementById('dynamic-title')
let dynamic = document.getElementById('dynamic')

let sections = new Map()
sections.set('menuBtn', menuBtn)
sections.set('closeBtn', closeBtn)

sections.set('menuSection', menuSection)
sections.set('tagsSection', tagsSection)
sections.set('categoriesSection', categoriesSection)
sections.set('searchSection', searchSection)
sections.set('dynamicSection', dynamicSection)

// pb ici quand on a pas de .html
let isLocal = /(.+\/)[^\/]+\.html/.exec(window.location.href)
let urlPart = ""
if (isLocal !== null) {
    urlPart = getURLPart(/(.+\/)[^\/]+\.html/)
} else {
    urlPart = window.location.href + "/"
}

let isArticle = /(\/article\/)/.exec(window.location.href) !== null

let index = ""
if (!isArticle) index = "article/"

let windowLocation = "articles"

/////////////
// Methods //
/////////////

/**
 * Create html event
 * @param type string
 * @param params Object {"content": string, "id": string, "class": string, "additions": [element], "stringAttributes": {name(string): value(string)}}
 * @returns HTMLElement
 */
function elt(type: string, params: Element) {
    let HTMLElt = document.createElement(type);
    if (params.content !== undefined) HTMLElt.textContent = params.content;
    if (params.id !== undefined) HTMLElt.setAttribute("id", params.id);
    if (params.class !== undefined) HTMLElt.className = params.class;
    if (params.innerHTML !== undefined) HTMLElt.innerHTML = params.innerHTML;
    if (params.additions !== undefined && Array.isArray(params.additions)) {
        params.additions.forEach(element => {
            HTMLElt.appendChild(element)
        });
    }
    if (params.stringAttributes !== undefined) {
        for (const [key, value] of Object.entries(params.stringAttributes)) {
            HTMLElt.dataset[key] = value.toString();
        }
    }

    return HTMLElt;
}

function icon(name: string, size: number = 1, type: string = "s") {
    return elt("p", {"class": "icon", "additions": [
        elt("i", {"class": `fa${type} fa-${name} fa-${size}x`} as Element)
    ]} as Element);
}

function parse(text: string) {
    let summary = document.getElementById('summary')
    let lines = text.split('\n')
    let divResult = document.createElement("div")

    let result = getAllClassifiedLine(lines)

    if (result[0].type !== "h1") {
        result.shift()
    }

    for (let i = 0; i < result.length; i++) {
        if (result[i].type === "ul" || result[i].type === "ol") {
            let list = getAllList(result, i)
            divResult.appendChild(displayList(list.list))
            i = list.id
        } else if (result[i].type === "co") {
            let code = getCodeBlock(result, i)
            divResult.appendChild(displayCode(code.list))
            i = code.id
        } else if (result[i].type === "no") {
            divResult.appendChild(elt("div", {"class": "note", "additions": [
                elt("p",{"innerHTML": parseLine(result[i].value)} as Element)
            ]} as Element))
        } else if (result[i].type !== "split") {
            let element = elt(result[i].type, {"innerHTML": parseLine(result[i].value)} as Element)
            if (result[i].type.startsWith("h")) {
                let link = document.createElement("a")
                link.innerHTML = parseLine(result[i].value)
                link.href = `#${i}`

                link.addEventListener("click", () => {
                    displayOrHideElements(["menuBtn", "summaryBtn"], "articles")
                })

                summary.appendChild(link)
                element.id = i.toString()
            }
            divResult.appendChild(element)
        }
    };

    return divResult
}

function getAllClassifiedLine(lines: Array<string>) {
    let result = [] as Array<Line>
    let listIteration = 0

    for (const line of lines) {
        let findRegex = false;

        for (const regex of allRegex) {
            
            let element = regex["rule"].exec(line);
    
            if (element) {
                findRegex = true;
                if (regex.type === "ol" || regex.type === "ul") {
                    result.push({"id": listIteration, "value": element[1], "level": regex.level, "type": regex.type, "rule": /a/});
                    listIteration++
                } else {
                    result.push({"id": 0, "value": element[1], "level": 0, "type": regex.type, "rule": /a/});
                    listIteration = 0
                }
                break
            }
        }

        if (!findRegex && line.trim().length !== 0) {
            result.push({"id": 0, "value": line, "level": 0, "type": "p", "rule": /a/});
            listIteration = 0
        } else if (line.trim().length === 0) {
            result.push({"id": 0, "value": "", "level": 0, "type": "split", "rule": /a/});
            listIteration = 0
        }
    }
    return result
}

function getAllList(list: Array<Line>, position: number) {
    let listElt: Array<Line> = []
    let i = position
    
    for (i; i < list.length; i++) {
        if (list[i].type === "ul" || list[i].type === "ol") {
            listElt.push(list[i])
        } else {
            break
        }
    }
    return {
        "id": i,
        "list": listElt
    }
}

// Méthode d'initialisation de la liste
function displayList(list: Array<Line>) {
    let listElt = elt(list[0].type, {} as Element)
    
    for (let i = 0; i < list.length; i++) {
        if (list[i].level === 0) {
            listElt.appendChild(displayListElt(list, i))
        }
    }
    
    return listElt
}

// Fonction recursive qui vise à afficher un élément et qui fait appel à une autre fonction pour récupérer ses enfants
function displayListElt(list: Array<Line>, eltId: number) {
    let li = elt("li", {"innerHTML": parseLine(list[eltId].value)} as Element)
    let children = getChildren(list, eltId, list[eltId].level + 1)

    if (children.length > 0) {
        let subList = elt(children[0].type, {} as Element)
        children.forEach(child => {
            subList.appendChild(displayListElt(list, child.id))
        });
        li.appendChild(subList)
    }
    return li
}

// Fonction qui donne tous les enfant direct d'un élément
function getChildren(list: Array<Line>, previousId: number, childrenLevel: number) {
    let children = [] as Array<Line>
    
    // Pour chaque itération de la liste entre le suivant (previous + 1) avec un maximum de
    for (let i = previousId + 1; i < list.length; i++) {   
        // Si l'élément suivant (previous + 1) a un niveau inférieur ou égal alors ce n'est pas un enfant
        if (list[i].level <= list[previousId].level) {
            break
        }
        if (list[i].level === childrenLevel) {
            children.push(list[i])
        }
    }

    return children
}

function getCodeBlock(list: Array<Line>, position: number) {
    let listElt: Array<Line> = []
    let i = position
    
    for (i; i < list.length; i++) {
        if (list[i].type === "ce") {
            break
        } else {
            listElt.push(list[i])
        }
    }
    return {
        "id": i + 1,
        "list": listElt
    }
}

function displayCode(list: Array<Line>) {
    let listElt = elt("section", {"class": `code ${list[0].value}`} as Element)
    
    for (let i = 1; i < list.length; i++) {
        listElt.appendChild(elt("p", {"content": list[i].value} as Element))
    }
    
    return listElt
}

function parseLine(line: string) {
    regexElt.forEach(regex => {
        let ifRegex
        
        while (ifRegex !== null) {
            ifRegex = regex["rule"].exec(line)

            if (ifRegex) {
                line = line.replace(regex.rule, regex.tag)
            } else {
                ifRegex = null
            }
        }
    });

    return line
}

async function getURLContent(url: string, fct: Function = null) {
    await fetch(url, {
        method: "get",
        headers: {"Content-Type": "application/json"}
    })
        .then(response => response.text())
        .then((content) => {
            if (fct !== null) fct(content)
        })
}

function getURLPart(regex: RegExp) {
    return regex.exec(window.location.href)[1]
}

function displayText(text: string) {
    document.getElementById("main").appendChild(parse(text))
}

function sortByDateDesc(array: Array<Link>) {
    array.sort(function (a, b) {
        if (a.date < b.date) {
            return 1;
        }
        if (a.date > b.date) {
            return -1;
        }
        return 0;
    });
}

function displaySummary(file: string) {
    let id = /=(.+)/.exec(file)[1].trim().replace(" ", "_").toLowerCase() + "_" + windowLocation
    let HTMLElement = document.getElementById(id)
    // HTMLElement.innerHTML = ""
    let summary = /```summary\n([^`]+)\n```end/.exec(file)[1]

    HTMLElement.appendChild(elt("p", {"content": summary} as Element))
}

function displayArticle(article: Link, location: string) {
    let link = document.createElement("a")
    link.href = `${urlPart}${index}${article.url}`
    link.id = article.name.trim().replace(" ", "_").toLowerCase() + "_" + location
    link.className = "link"

    let title = document.createElement("p")
    let hr = document.createElement("hr")
    let tags = elt("div", {class: "tags"} as Element)
    title.innerHTML = `<span>${article.category} :</span> ${article.name}`
    article.tags.forEach(tag => {
        tags.appendChild(elt("p", {class: "chip", content: tag} as Element))
    });
    link.appendChild(title)
    link.appendChild(hr)
    link.appendChild(tags)
    
    getURLContent(`${urlPart}${index}${article.url.replace(/.html/, '.adoc')}`, displaySummary)
    return link
}

function displayTree(tree: string) {
    let articleResultArray: ResultArray = {tags: new Map([]), categories: new Map([]), articles: []}
    
    let date = localStorage.getItem("date")

    // Si il n'y a rien enregistré dans le local storage ou que la date n'est pas identique
    if (date === null || date !== tree.split('\n').shift()) {
        localStorage.setItem("date", tree.split('\n').shift());

        let treeArray = tree.split('\n')
        for (let i = 1; i < treeArray.length; i++) {
            if (treeArray[i] !== "") {
                let url = `${(/(.+).adoc/.exec(treeArray[i]) ?? ["","../index"])[1].trim()}.html`
                let name = ((/=(.+)\|/.exec(treeArray[i]) ?? ["","Aucun nom"])[1]).trim()
                let date = ((/\|([^|:]+):/.exec(treeArray[i]) ?? ["","Aucune date"])[1]).trim()
                let type = ((/:([^|:]+):/.exec(treeArray[i]) ?? ["","Aucune catégorie"])[1]).trim()
                let tags = ((/:(?:.+):([^:]+)/.exec(treeArray[i]) ?? ["","Aucun"])[1]).replace(/\s/g, '').split(",")

                let link = {name: name, url: url, date: date, tags: tags, category: type}

                articleResultArray.articles.push(link)
                
                for (const tag of tags) {
                    let array = articleResultArray.tags.get(tag)
                    if (array === undefined) {
                        array = []
                    }
                    array.push(link)
                    articleResultArray.tags.set(tag, array)
                }

                let category = articleResultArray.categories.get(type)
                
                if (category === undefined) {
                    category = []
                }
                category.push(link)
                articleResultArray.categories.set(type, category)
            }
        }

        //////////
        // Tags //
        //////////

        // Order tags alphabetically
        articleResultArray.tags = new Map([...articleResultArray.tags.entries()].sort());

        articleResultArray.tags.forEach((tag, key) => {
            // Order articles in tag by date DESC
            sortByDateDesc(articleResultArray.tags.get(key))

            let chip = elt("div", {
                "class": "chip",
                "additions": [
                    elt("p", {"content": `${tag.length}`, "class": "label"} as Element),
                    elt("p", {"content": key, "class": `tag-${tag.length}`} as Element)
                ]
            } as Element)

            chip.addEventListener("click", () => {
                dynamicIcon.className = "fas fa-tags fa-2x"
                dynamicTitle.innerHTML = key
                dynamic.innerHTML = ""

                articleResultArray.tags.get(key).forEach(article => {
                    dynamic.appendChild(displayArticle(article, "tags")) //ici
                });
                displayOrHideElements(["menuBtn", "dynamicSection", "closeBtn"], "tags")
            })
            document.getElementById("tags").appendChild(chip)
        });

        ////////////////
        // Categories //
        ////////////////

        // Order categories alphabetically
        articleResultArray.categories = new Map([...articleResultArray.categories.entries()].sort());

        articleResultArray.categories.forEach((category, key) => {
            // Order articles in tag by date DESC
            sortByDateDesc(articleResultArray.categories.get(key))

            let chip = elt("div", {
                "class": "chip",
                "additions": [
                    elt("p", {"content": `${category.length}`, "class": "label"} as Element),
                    elt("p", {"content": key, "class": `tag-${category.length}`} as Element)
                ]
            } as Element)

            chip.addEventListener("click", () => {
                dynamicIcon.className = "fas fa-layer-group fa-2x"
                dynamicTitle.innerHTML = key
                dynamic.innerHTML = ""

                articleResultArray.categories.get(key).forEach(article => {
                    dynamic.appendChild(displayArticle(article, "categories"))
                });
                displayOrHideElements(["menuBtn", "dynamicSection", "closeBtn"], "categories")
            })
            document.getElementById("categories").appendChild(chip)
        });

        //////////////
        // Articles //
        //////////////

        // Order articles by date DESC
        sortByDateDesc(articleResultArray.articles)

        if (!isArticle) {
            articleResultArray.articles.forEach(article => {
                let main = document.getElementById("main")
                main.appendChild(displayArticle(article, "articles"))
            });
        }

        // console.log(JSON.stringify(articleResultArray));
        
        localStorage.setItem("articles", JSON.stringify(articleResultArray.articles));
        localStorage.setItem("tags", JSON.stringify(articleResultArray.tags, replacer));
        localStorage.setItem("categories", JSON.stringify(articleResultArray.categories, replacer));
        // console.log("oh no");
    } else {
        // console.log("youpi");
        
        articleResultArray = JSON.parse(localStorage.getItem("tags"), reviver)
    }

    // console.log(articleResultArray);
}

function replacer(key: string, value: Map<string, Array<Link>>) {
    if(value instanceof Map) {
      return {
        dataType: 'Map',
        value: Array.from(value.entries()), // or with spread: value: [...value]
      };
    } else {
      return value;
    }
  }

  function reviver(key: string, value: Map<string, Array<Link>>) {
    if(typeof value === 'object' && value !== null) {
    //   if (value.dataType === 'Map') {
        // console.log(key);
        // console.log(key.value);
        let test = new Map()
        test.set(key, value)

        // console.log(test);
        
        
        return test;
    //   }
    }
    return value;
  }

function displayOrHideElements(elementsToDisplay: Array<string>, location: string = "") {
    windowLocation = location
    sections.forEach((section, key) => {
        if (elementsToDisplay.includes(key)) {
            section.style.display = "flex"
        } else {
            section.style.display = "none"
        }
    })
}

//////////
// Code //
//////////

menuBtn.addEventListener("click", () => {
    displayOrHideElements(["menuSection", "closeBtn"], "menu")
})

closeBtn.addEventListener("click", () => {
    displayOrHideElements(["menuBtn", "summaryBtn", "commentoBtn"], "articles")
})

aboutBtn.addEventListener("click", () => {
    console.log("about");
    // displayOrHideElements(["menuBtn", "closeBtn", "searchSection"], sections)
})

searchBtn.addEventListener("click", () => {
    displayOrHideElements(["menuBtn", "closeBtn", "searchSection"], "search")
})

categoriesBtn.addEventListener("click", () => {
    displayOrHideElements(["menuBtn", "closeBtn", "categoriesSection"], "categories")
})

tagsBtn.addEventListener("click", () => {
    displayOrHideElements(["menuBtn", "closeBtn", "tagsSection"], "tags")
})

if (!isArticle) {
    getURLContent(`${urlPart}article/tree.txt`, displayTree)
} else {
    getURLContent(`${urlPart}${getURLPart(/\/([^\/]+).html/)}.adoc`, displayText)
    getURLContent(`${urlPart}tree.txt`, displayTree)

    let homeBtn = document.getElementById("home-btn")

    homeBtn.addEventListener("click", () => {
        document.location.href="../index.html";
    })

    let summaryBtn = document.getElementById("summary-btn")
    sections.set('summaryBtn', summaryBtn)
    let summarySection = document.getElementById('summary-section')
    sections.set('summarySection', summarySection)
    summaryBtn.addEventListener("click", () => {
        displayOrHideElements(["summarySection", "closeBtn", "menuBtn"], "summary")
    })
    
    let commentoBtn = document.getElementById('commento-btn')
    sections.set('commentoBtn', commentoBtn)
    let commentoSection = document.getElementById('commento')
    sections.set('commentoSection', commentoSection)    
    commentoBtn.addEventListener("click", () => {
        displayOrHideElements(["commentoSection", "closeBtn"], "comment")
    })
}
