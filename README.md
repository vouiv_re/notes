# Notes

[vouiv.re](https://vouiv.re/)

To install the project locally:

```sh
npm install
```

To run the project locally:

```sh
npm run all
```

This command run:

* a lite server
* TS compilation and watcher
* SASS compilation and watcher

To generate .html file from .adoc file run:

```sh
./create_html.sh 
```

To generate .adoc fake article run:

```sh
./create_adoc.sh 
```

```sh
asciidoctor-pdf file.adoc
```
