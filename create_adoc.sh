#!/bin/bash

day=0
month=0
year=0

for (( c=0; c<4; c++ ))
do
    filename=$(./lorem_ipsum/lorem_ipsum.sh -w 1 -no)

    filename="${filename//[[:blank:]]/}"
    filename="${filename%.*}"
    filename="$(tr '[:upper:]' '[:lower:]' <<< "${filename}")"

    touch "./public/article/$filename.adoc"

    title="$(tr '[:lower:]' '[:upper:]' <<< "${filename:0:1}")${filename:1}"

    if [ $day -ge 10 ]
    then
        day=1
        ((month++))
    else
        ((day++))
    fi

    if [ $month -ge 10 ]
    then
        month=1
        ((year++))
    else
        ((month++))
    fi

    {
        echo "197$year-0$month-0$day:article: jeu, top-down, rogue-like"
        echo "= $title"
        echo ""
        echo "$(./lorem_ipsum/lorem_ipsum.sh -p 1 -no)"
    } > "./public/article/$filename.adoc"
done

/bin/bash ./create_html.sh