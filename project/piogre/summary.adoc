= Récapitulatif des règles

* Compétence = 1d20 + Compétence + Caractéristique
* Défense = 10 + Caractéristique + Bonus
* Accumulation [X, trait] quand l'accumulation est égale à X le trait s’applique au début du tour de la cible.
* Saturation = La cible subit l’état pendant x + 1 tours, x étant le nombre de pas de cinq dépassants le seuil du ND.