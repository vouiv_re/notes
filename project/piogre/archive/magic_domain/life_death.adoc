= Domaine de la vie et de la mort

** [det] Trouver les morts : Jeter 1d6 pour savoir combien il y a des squelettes dans la région, puis 1 action d'Esprit par corps que le joueur veut localiser, puis jeter 1d8 et 1d12 pour le placer sur la carte, puis jeter 1d pour savoir si c'est un humanoïde, un monstre ou un animal
** [alt] Exosquelette +3 à la défense
*** (pan) Tenue chic
*** (pui) +6 à la défense
** [alt](pui) Fusion des morts
** [alt] Automutilation
** [alt] Griffe d'os
** [alt] Soin
** [alt] Resurrection
** [alt] Réveiller les morts : 1 action majeur d'Esprit + 1 action en concentration par tour et par créature contrôlé
** [inv] Entrave thoracique
** [inv] Transfert de douleurs
** [inv](pan) Dialogue avec les morts
** [inv] Sagaie/arme d'os
** [inv] Nuage de poison

== Enchantement

* Rune de fusion des morts : permet d'augmenter ses points de corps et éventuellement sa taille.

== Traits

* Mort-vivant : N'a plus d'endurance
* Drain de vie : Sur une action qui provoque une blessure permet de soigner une blessure 