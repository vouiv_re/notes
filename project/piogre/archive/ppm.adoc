= Petit Précis de Magie

Il existe cinq types de magie :

* L'altération
* L'invocation
* L'enchantement
* L'alchimie
* La divination

Il existe trois types d'accès à la magie :

* Le sernent (entité, dieu)
* Le vœu (religion, nature, communauté)
* Le talent

Il existe quatre sources de la magie :

* Les entités
** Les dieux
** Les monstres (Magamoth)
* La nature
** Les arbres druidique
** Les roches
* Les astres
* Le plan éthéré

La magie vient du plan éthéré, c'est son but et raison d'être. Des lieux, des entités et des objets sont naturellement lié à ce plan on les appelles les sources.

Chaque être vivant, non-amagique, à la capacité d'engranger, de stocker cette ressource.

Une religion peut donner un accès à sa source éthéré ou son dieu peut donner un accès direct.

== Religion

Meunière.

Les dieux sont des porteurs fragments. Il sont donc des sources. Ce sont des créatures, entités, individus qui ont absorbé un fragment d'ether et qui ont survécu.

Pour survivre à l'absobtion il faut être détenteur d'une idée forte, d'une volonté claire. C'est cette idée qui deviendra l'attribut du futur dieu. L'individu se transforme alors pour devenir un archétype dont les pouvoirs et les capacité sont tout entier au service de cette idée.

DD 25 de la compétence liée.

On peut utiliser un accumulateur 3. 3 tours au dessus de 10 donne un bonus de +1, en dessous -1, les critiques sont de 2.

Un fragment peut déjà être teinté de la volonté d'un précédent dieu.

Un dieu peut ne plus croire en son idée, il dépérit alors.

Un dieu s'entoure pour propager sa cause de fidèles, de prêtres et de champions. Ces deux derniers disposent d'écharde d'ether. C'est une petite partie du pouvoir du dieu.
 
Pour tuer un dieu il existe un rituel qui met en scène un affrontement. Celui-ci peut être pour la forme pour une transmission ou pas.
 
 On peut faire changer un fragment de but.

== Alchimie

Un alchimiste utilise les principes actifs présent dans les ingrédients (d'origine végétale, animale, minérale) en exacerbant leurs effets pour fabriquer des objets au pouvoir ravageur.

Les ingrédients proviennent de ressources.

* Plantes
** Écorce
** Sève
** Racine
** Feuille
** Fleur
** Bourgeon
** Tige
* Animaux
** Poil
** Écaille
** Dent, griffe
** Os
** Sang
** Organes (foie, cœur, glande, poumon)

Les quantités et les méthodes d'extraction peuvent varier selon les ressources.

Le joueur lance 1D20 et obtient X + son bonus de compétence d'alchimie ingrédients. X étant le nombre d'itération au pas de cinq du résultat du jet.

Il choisit les ingrédients qu'il garde.

Il existe des atténueurs, des préventifs, des antidotes, des anti-poison et des anti-douleurs.

L'alchimiste doit tester les ingrédients pour connaître leurs effets.

* Goûter (humain. animal)
* Tester sur des objets

Certains ont des effets dans leurs état naturel, d'autre seulement après transformation.

Transformation :

* Macération
* Dilution
* Distillation

Il peut avoir besoin de plusieurs solutions :

* Eau
* Huile

Un même ingrédients peut avoir plusieurs effets. Il peut vouloir les séparer pour éviter les effets secondaire.

Des effets peuvent être fort ou faible. Un effet fort suffira à faire une potion. Un même effet faible servira seulement à l'améliorer.

Des effets secondaire peuvent être ajouter pour améliorer la potion :

* Retardateur
* Durée
* Goût (chaque ressource à un goût particulier qui peut être masqué)
* Couleur

Il y a des familles de fleurs qui réagissent de la même manière et/ou ont des effets semblable.

Un alchimiste peut faire 3 mélanges/préparations par heure + son bonus de compétence. Sinon c'est 10 minutes.

Piste : Avec la magie de l'eau il pourrait accélérer les dillutions. Pour faire des effets en combat.

* Bombe d'argile
* Potion
* Bombe d'argile avec retardateur
* Fumigène
* Feux d'artifice
* Encens : résine qui se consume
* Infusion

Un effet des ingrédients peut être le colorant.

Les effets peuvent avoir des conditions pour s'activer :

* Bu
* Ingérer
* Choc
* Inhaler

== Objets

* Taille (encombrant)
* Poids (Lourd)
* Qualité
* Valeur
* Effet

== Vocabulaire

* Magifuge : Qui ne craint pas la magie, imperméable
* Amagique : Qui n'a aucune prédisposition à la magie
* Inapte : Qui n'a pas la capacité de maitriser la magie