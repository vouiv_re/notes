= Alternatives

== Création de personnage

=== Alternative

Un aventurier un peu expérimenté commencera en plus avec :

* 1 compétence au niveau 3
* 2 compétences au niveau 2

== Gain de niveau

=== Alternative 1

Le joueur suit cet ordre qui lui indique l’orientation de son gain de niveau :

* Compétence
* Action spéciale
* Compétence
* Trait
* Légendaire (dans cet ordre)
** Champion
** Caractéristique
** Intention
** Virtuose

Arrivée à la fin, il recommence au début.

=== Alternative 2 : jeu avec un joueur

Il ne peut découvrir une nouvelle compétence qu'auprès d'un maitre ou d'un livre. En faisant un jet de connaissance/compréhension  pas de 5 lui donne le nombre de jour dans lequel il l'obtiendra. il pourra alors faire les jets sans DVT. Mais il sera à 0. Tous les échecs il augmente de 2 sa jauge d'apprentissage toutes les réussites il augmente de 1. À 10 il gagne un point dans sa compétence. Pour démontrer que l'on progresse plus vite par ses échec que par ses réussite.

WIP : Le personnage peut passer son niveau pour apprendre une connaissance ou une connaissance rencontré lors de ses aventures.

== Saturation

La cible subit l’état pendant x + 1 tours, x étant le nombre de pas de cinq dépassants le seuil du ND.

== Sortilèges

* Lancer le sortilège à distance coûte 1 PA E
** longue, coûte 2 PA E
** de région, coûte 3 PA E
** infinie, coûte 4 PA E

***

```markdown
Exemple :

* Rapide
 * Obtient un AVT à l'initiative
 * Sur un jet d’athlétisme, donne le trait véloce
```