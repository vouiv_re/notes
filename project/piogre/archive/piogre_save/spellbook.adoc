# Grimoire

* Flèche de feu
** Donne l'état enflammé sur un DD 15
** 3 dégâts physique
** Porté courte
** Ligne droite
** Coûte 2 point d'esprit
* Projectile magique
** Autant de projectile que d'esprit
** 1 dégât d'esprit par projectile
** Tête chercheuse
** Coûte autant que de flêche, adaptable
* Rayon de givre
** Gel la cible sur 15+
** Ligne droite
** 1 dégât de Corps
** cote 2 d'esprit