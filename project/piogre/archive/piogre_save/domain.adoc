= Domaine

== Général

* Lent (Peut se déplacer max de 3 cases)
* Terrain de prédilection
** fôret/plaine/marais/lacustre/montagne/steppe/desert/ville/sous-marin
*** discrétion
*** observation
*** Connaissance des plantes
*** Connaissance des animaux
* Piège
* Vision
** Nyctalope : vision dans la nuit, pas de désavantage
** Vision parfaite dans les ténèbre : vision dans le noir total, pas de désavantage