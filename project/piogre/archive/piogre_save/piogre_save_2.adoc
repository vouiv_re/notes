= PIOGRE

== Niveau de difficulté

* Réussite automatique
* Aisé (DD 5+)
* Facile (DD 10+)
* Réalisable (DD 15+)
* Difficile (DD 20+)
* Ardu (DD 25+)

== Intention

Une *intention* est une manière de typer une action.

```
Exemple :
- Un barde donne une représentation : il joue donc de la musique avec panache.
- Un sniper vis la tête d'un malfrat avec son fusil à lunette : il tire à distance avec précision.
```

Utiliser une *intention* coûte une action supplémentaire.

Tout le monde peut utiliser n'importe quel intention, mais s'il ne possède pas le trait correspondant ça augmente la difficulté de l'action au ND supérieur.
Le jet est réussis si il se situe entre le ND initial et le ND supérieur mais l'intention à échoué.
On peut choisir plusieurs intentions ça augmente d'autant la difficulté.

ex : défense de 17 je passe à 20. Crochetage à 10 je passe à 15.

* discret(prudent?)
* rapide
* précis/subtil
* puissant
* panache(bluff/provocation)

== Création de personnage

* 3 points à répartir entre le corps et l'esprit
* On choisit 1 intention
* Choix de 3 compétences

== Endurance et blessures

* Tous les personnage ont 3 d'endurances.
* Quand le personnage est touché il perd 1 d'endurance
* Quand le personnage est touché et qu'il a 0 d'endurance il est blessé.
* Pour une action il peut récupérer 1 point d'endurance pour une limite de X fois par repos. X étant son score de corps.
* Un personnage peut subir X blessures. X étant son score de corps. Au delà la prochaine blessure le met hors combat et ou mort.

> Ces règles son applicable pour l'esprit. il faut remplacer l'endurance par la volonté. ??? pas sur faire juste réduire l'endurance.

== Règles

=== Test

Le joueur jette un dé pour décider de l'issue d'une action de son personage quand sa réussite n'est pas assurée

Il jette un dé à 20 face (D20) et ajoute son bonus de caractéristique et son bonus de compétence. Il le compare alors soit à un niveau de difficulté (ND) choisi par le meneur soit à la défense de l'objet de son action (10 + caractéristique + autre bonus)

=== Avantage / Désavantage

* Quand un personnage dispose d'un avantage (Avt) il lance deux dés il garde le résultat le plus élevé.
* Quand un personnage subit un désavantage (Dvt) il lance deux dés il garde le résultat le plus faible.

Les avantages et désavantages subviennent quand le personnage agit dans un contexte particulier :

* Un alchimiste fabrique une potion dans un laboratoire tout équipé => Avt
* Deux rôdeurs s'entraident pour pister une créature, comme ils partagent une compétence commune => Avt
* Le voleur s’échappe par les toits, manque de chance il pleut => Dvt
* Le guerrier est tombé au sol, dans un ultime sursaut d’énergie il tente de donner coup à son adversaire => Dvt

== Compétence

=== Corps

* Combat à l'arme de corps à corps
// * Pugilat
// * Projection
* À main nue : art martiaux de manipulation : maintenir, bloquer, mettre au sol, entraver, jeter sur un obstacle ou sur qqn, jeter dans le vide
* Combat à l'arme improvisé (càc et distance)
// * Manœuvre (charge, mettre à terre, faire reculer, désarmer, échanger de place (attaque sournoise), entraver, assommer (seulement qqn qui n'est pas sur le qui-vive ou qui n'a plus d'endurance))

* Combat à l'arme à distance (Arc, fronde, arbalète)
* Combat à l'arme de lancé (Dague, grenade)

* Crochetage (serrure, pose de piège, désamorçage)

* **Athlétisme** : course, natation, escalade, ramper, acrobatie

* [C] Piloter

// * **Furtivité**, se cacher /// Avec le trait d'intention discret pas sur que ce soit utile. le mec fait des atq furtive quand il rate est repéré. La question se pose quand il veut se cacher. Athlétisme + discret ?
* Vol à la tire

* Mettre en scène/Représenter (spectacle, conte, musique, chant, représentation)
* **Intimider**
* Dressage
* Déguiser

=== Esprit

* [F] Alchimie
* [F] Altération
* [F] Invocation
* [F] Détection (magique)
* [F] Enchantement

* Soigner : médecine
* **Connaître/comprendre** : identifier, comprendre les caractéristique d'un objet, perspicacité, connaissance, intuition

* [C] Hacker

* **Persuader**, charmer, séduire, mentir, convaincre
// * **Prévoir** : Permet de deviner la prochaine action d'un individu ??? Trop spécifique, à regrouper
// * **Analyser** : Permet de découvrir les forces et faiblesse d'un individu, savoir si notre interlocuteur nous ment, idem pour percevoir ?
* **Percevoir** : Trouver un piège, repérer quelqu'un qui se cache, trouver un objet
* **S'orienter/se repérer**
* **Enseigner/transmettre/communiquer**

=== Neutre

=== Variable

* Artisanat : métier

=== Domaine magique

* Feu
* Air/Gaz
* Eau
* Minéral
* Végétal
* Animal
* Électricité
* Lumière
* Attraction
* Onde
* Température
* Poison
* Météo
* Esprit
* Mort
* Artificiel
* Espace
* Temps
* Inertie
* Éthéré

* Générique

== Actions

Il y a des actions et des actions rapides (AR). Une AR équivaut à une demie action.

On a 3 actions par tour. On peut en faire plus mais ça puise dans l'endurance.

* Attaque
* Manœuvre
* Désarmer
* Esquive
* Feinte
* Pousser
* Ceinturer
* Assommer
* Lancer un sortilège
* Changer d'armes (dégainer ET rengainer)
* [AR] Parler (6 mots par actions rapide)
* [AR] Boire une potion
* [AR] dégainer OU rengainer

== États

* Inconscient
* Entravé
* Paralysé
* Enflammé
* Aveuglé : Désavantage au jet d'opposition
* Gelé

== Traits

* Endurant
* Maîtrise d'arme
* Soiffard
* Leste