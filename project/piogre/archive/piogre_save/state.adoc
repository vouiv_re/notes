= États

* Aveuglé : Ne peut atteindre une cible que sur un coup critique
* Enflammé : inflige 1 dégât d'esprit et de corps au début du tour de l'individu tant que enflammé
* Geler : -1 action de corps par tour
* Empoisonner : -1 point d'endurance par tour ou il subit le poison, cumulable (Quelqu'un avec 5 de poison subira 1 dégât pendant 5 tour)
* Bâillonné : Ne peut rien prononcer
* Paralyser : Ne peut plus bouger
* Immobiliser : Ne peut plus se déplacer

== Vulnérabilité

* vulnérable : double dommage et ou état
* résistant : moitié dommage
* immunisé : 0 dommage
* affinité / biotope : buff ou soin

== Élément

> pour enlever un état du à un élément, il faut consommé une action

* Feu : + 1 dégât supplémentaire chaque tour
* Glace : 1 dégât et 1 action en moins, se cumule, plus on est glacé, moins on peut agir
* Poison