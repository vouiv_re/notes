= Magie

Il y a deux écoles de magie, l'école du verbe et celle du geste, chaque lanceur doit choisir une des deux.

* Invocation = Parole
* Altération = Geste

Un sort en rituel coûte un de moins le tour d'après. Pour le déplacer ça coûte quand même 1 pts.
Un sort en rituel coûte toujours au moins 1 pour le maintenir, si la concentration est brisé (Jet sur l'esprit (trait volonté permet de garde sa concentration avec avantage))

Invoquer un familier coûte un point d'esprit majeur.
Pour le révoquer, il faut soit mettre hors d'état de nuire le lanceur de sort, soit qu'il le révoque (une action)n un invocateur sur une action de revocation réussie (si il a le même trait  peut révoquer la créature, jet en opposition ou jet sur la défense d'esprit d'un invocateur)
Les actions majeurs ne jette pas de dés
il ne ponctionne pas de point d'action par tour, ce n'est pas un rituel
Le temps d'incantation prend le tour entier

Certains sorts n'ont pas besoin de test pour être lancer hors combat ? => lumière, allumer une surface.

Trait: permet de lancer des contre-sort pendant une réaction

Faire une mécanique avec les contre-sort ?
Les arcanes permettent  de lancer un sort en différé, de faire des balises, et des counter spell, de faire des sorts plus puissant, sont ce des traits plutôt qu'une compétence ? => Oui

== Domaines

* Feu
* Esprit, contrôle
* Onde : Message Illusion sonore, Sort de silence (Alt)
* Gravité : Télékinésie, repousser, attirer, vol
* Glace = eau + froid = Altération eau : retire des actions, retire des mouvements retire des actions de corps
* Poison - d'action d'esprit, empêche de régénérer
* Animal, métamorphose (alt), invocation
* Plante, racine ralentissement (alt) mur, épine, fouet arme invoquer
* Eau : Dégât + éteint le feu, glisse
* Minéraux = Pierre + Terre (Création de mur, Golem) + Métal, armure de mage (alt), pétrole ? Sable mouvant (alt) Sur 20+ le coup assomme
* Lumière = Couleur/Oeil, illusion invisibilité, aveuglement
* Arcanes = permet de faire de l'antimagie, et des contre sort, et de servir de support à d'autres sort, de décaler ses lancements de sorts, d'augmenter sa puissance

Repérer des puis magique, qui donne plus de point d'actions d'esprit

L'idée c'est d'avoir x malus = défense -2, on peut donc créer autant d'armure qu'on le souhaite

* carbone/medulith/exosquelette/stormtroopers? Cuir +1
* Besantine +2 + D Action magique somatique mvt
* Maille +3 D Athlétisme  + D Action magique somatique mvt
* Écaille +4 D Athlétisme + D discrétion
* Plate +5 D Athlétisme - 2 mvt + D discrétion


Grav(ité) suspenseur, chute controlé
fulgurant
dephaseur
laser
plama
nanolame
fumigène
grenade uem
scaphandre de combat

Amélioration des arcanes :

* Pouvoir faire des balises de sort
* Pouvoir stocker du mana pour plus tard

***

Sort de contrôle en opposition sur la défense mentale. parce que la personne est passive, à la difference d'un jet en opposition sur deux compétences, comme par exemple un bras de fer.

***

Sorts de froid :

* Ralentir
* [Alt] Geler une surface d'eau
* Flèche de glace
* Mur de glace, pont sur une surface d'eau
* Éteindre un feu

Sort d'électricité :

* rebondis à travers les entités  (1 point par exclusion)

Sorts temporel

* Refaire son tour
* Relancer un jet (allié/ennemis)

Sort d'esprits

* Contrôle mental
* Empêcher de voir
* Donner des ordres

Sorts d'eau :

* Éteindre
* Mouiller
* Glisser
* Flèche d'eau

Sorts animal :

La métamorphose  complète n'est pas un rituel, il coûte le tour, une action majeure. Qu'est-ce qui pourrait coûter plus pour une métamorphose qui change la taille. Peut-être rien.

Un trait par groupe d'espèce animale, canidé, félin, ...
Ou mammifère, reptile, oiseaux, poisson.

Méduse a beaucoup d'esprit et peu lancer des sorts.

Pendant la métamorphose on garde sa conscience mais on prend l'esprit de l'hôte.

Règles d'hybridation

* [Alt] Communication animale
* [Alt] Charme animal / Colère, peur
* [Inv] Fais apparaître des animaux
* [Alt] Métamorphose partielle, totale
* [Alt] Possession animale
* [Alt] Hybridation animale (Hibours, centaure)

Sorts végétal :

* [Inv] Fait apparaître une plante/un arbre
** Une fleur empoisonnée
* [Inv] Eclat de bois
* [Inv] Liane, fouet d'épine
* [Alt] Enracinement: Fait croître les racine pour entraver quelqu'un
* [Alt] Armure d'écorce

Sorts sonore :

* [Inv] Vacarme : rend sourd temporairement et étourdis
* [Inv] Message : permet de chuchoter aus oreilles de quelqu'un
* [Inv] Détourner l'attention: crée un son, un bruit, un cri, un rugissement
* [Alt] Porte voix : permet d'amplifier et de modifier sa voix ou un son
* [Alt] Zone de silence empêche les invocateurs de lancer un sort dans une zone
* [Ench] Glyphe d'alarme

Sorts de lumière :

* [Alt] Zone d'ombre (7 cases): Désavantages aux jet de touche + attaque furtive possible
* [Alt] Obscurité: désavantage au jet de touche
* [Inv] Aveuglement, désavantage au jet d'attaque
* [Inv] Lumière
* [Inv] Mur de lumière/obscurité, empêche l'assurance du ciblage
* [Inv] Illusion concentration
* [Inv] Création d'avatar concentration, crée x images du lanceur de sort, il partent tous du même endroit, ils ont autant de déplacement/d'"actions" que le personnage.
* [Inv] Illusion  créer une image pour se faire passer pour qqn d'autre qq chose d'autres
* [Inv] Balise lumineuse permet de marquer un emplacement au loin
* [Alt] être de lumière: difficile à cibler, désavantage aux jets de touche
* [Alt] Allumer une surface

Traits :

* Sort puissant: désavantage double effet
* Auto-cible: un coût supplémentaire touche automatique ? => Nope
* detection de la magie, avec bonus si on maîtrise le domaine

***

Mécanique sur un jet au D20

* Résultat du dé : hasard
* Bonus : Caractéristique + compétences + bonus objet
* Malus: : malus objet
* Avantage : Assistance, circonstance particulièrement favorable ??? (Attaque furtive, Attaque en tenaille, Attaque immobilisé)
* Désavantage : État défavorable, circonstance  défavorable, non maîtrise d'une compétence de spécialiste, nuit, météo
* Niveau de difficulté : Défense, appréciation du meneur, défense mentale ???

***

Faire des caractéristique fixes, avec un bonus au regain d'endurance pour les jeunes, mais moins de corps et d'esprit, de même pour les vieux.

Faire qu'on puisse changer une caractéristique par niveau

***

L'huile enflamme directement la case et double les dégâts.

Invocation :

* Invocation de flammes
** Main enflammé = Feu + Cône (petit/grand) = **2/3pts** (5/9 cases)
** Boule de feu = Feu + Trajectoire + Sphère = **3pts** (7 cases + trajectoire)
*** Trajectoire donc possiblement gêné par les obstacles, mais dégât tout le long
** Projectile enflammé = Feu + Trajectoire = **2pts** (1 case + trajectoire)
** Mur de feu de protection circulaire = Feu + Cercle = **2pts** (6 cases)
** Mur de feu à distance = Feu + Distance + Mur = **3pts** (5 cases)
** Mur de feu depuis soi = Feu + Mur = **2pts** (5 cases)
** Flamme = Feu + Distance = **2pts**
** Flamme ardente = Feu + Distance + Sphère = **3pts**
* Imp de feu (considéré comme tuile enflammé)
* Démon enflammé (considéré comme tuile enflammé)
* Arme enflammé

Altération :

* Insensibilité au feu
* Flamme indolore, qui ne brûle pas
* Enflammer arme

Relais de magie.

***

Colonne = Touche sur l'emplacement sur tout les espaces vides (cible aussi le ciel) une sphère touche 1 case en hauteur

Une cible qui a une vulnérabilité au feu s'enflamme automatiquement si elle est sur une case subissant un effet de feu.

Une cible est enflammé si l'attaque dépasse de 5 sa défense.

Une cible enflammé prend 1 dégât supplémentaire par case adjacente enflammé.

Traits :

* Discipline mentale : Le personnage peut retirer x case de la zone d'un sortilège x étant le nombre de point d'action dépensé supplémentaire
* Foyer du feu : être dans une case de feu permet de récupérer 1 pt d'endurance par tour

== Système

> L'addition des points de distances, zone, durée, déplacement, puissance de l'effet plus la soustraction du coût donne la puissance du pouvoir

* Distance
.. Soi
.. Contact
.. Courte portée (Arc, Fléchette) = proche
.. Longue portée (Baliste) = loin
.. N'importe ou
* Zone d'effet
.. Une cible
.. Un groupe
.. Une zone
*** Rayon
*** Sphère
*** Cône
.. Une région
.. Partout
* Durée
.. Un instant (moins d'une action, une action bonus)
.. Une action (et les réactions)
.. Une scène (concentration)
.. Une journée (buff)
.. Tout le temps (Enchantement)
* Déplacement
** Ramper, reptation
** Voler
*** Ligne droite
*** Drop
** Téléporter
** Rebond (éclair en chaîne)
** Par balise, point relais (Transmission de message)
** Écho
** Répartition, répandre, cercle concentrique
** Mimétisme
** Lien (électricité)
** Tourbillon
* Taille
** Minuscule
** Petit
** Moyen
** Grand
** Gigantesque
* Rapidité
** Traînard
** Lent
** Moyen
** Rapide
** Véloce
* Difficultés
** Aisé (DD +4)
** Facile (DD +8)
** Réalisable (DD +12)
** Difficile (DD +16)
** Ardu (DD +20)
* Temps d'incantation (rituel)
* Besoin
** Ressource
** Son
** Geste
** Concentration

Des sorts avec concentration pourrait être de plus en plus puissant.
La magie est neutre de tout effet de base, mais on peut lui en rajouter

=== Effets

> Chaque effet à un degrés de puissance

* Assommer, étourdir
* Désarmer
* Mettre à terre
* Entraver/immobiliser
* Élément
** Geler
** Enflammer
* Parler/Écouter
* Chercher
* Absorption
* Bouclier
* Aveugler
* Renvoi

=== Pouvoir

* Télépathie : Parler à distance à une cible
* Boule de feu

== Compétences

* Arcane

== Traits

* Incantation silencieuse, immobile pour un point d'action de plus
* Maître des éléments : Diminue le DD des effets de sort de 5

== Sorts

* Invocation de lames/flèches spectrale (Esprit)
** Pouvoir appliquer un effet à l'invocation (feu, glace, ...)
** A son tour faire une attaque en dépensant un point d'esprit pour faire une attaque avec une de ses lames
* Armure magique
* Téléportation
* Image miroir
* Confusion
* Contrôle mental
* Duplication
* Bouclier anti-magie
* Renvoie
* Parler aux morts

== Artefact

L'anti-magie re-transformerais un métamorphe
Un verrou magique empêcherais l'arrêt d'un sortilège, il ne pourrait cesser d'être transformer tant que le verrou est présent. Il pourrait épuiser quelqu'un aussi en l'empêchant d'arrêter son sort. Le verrou est un sort de concentration
Le maintien est un sort de soutien qui permettrai à quelqu'un de prêter des points d'esprit

== Illusionniste

Création d'image, de son d'odeur et de sensation.

C'est de l'altération de la vision, et des sens ou l'invocation des sens ou une école à part ?

Il peut prendre la forme de gens qu'il a vu, même si des détails peuvent le trahir, a part si il connaît très bien la personne
il peut cacher des objets provoquer des bruits, ...
