= Notes

== Atelier d'écriture

. Chaque auteur est le seul maître
. Entraide et conseil
. Défis ?
. story cubes 1/d4 d'h

== Antre de la vouivre

* soirée actual play
* mur avec décors modulaire
* jdr public école, prison
* son et décors
* studio
* enregistrement intervenant
* question : quel sont les écueils de l'associatif
* Faire du JDR la ou les gens attendent
** Gare
* Proposer L de faire un JDR solo por l'autre sur twitch

Attribuer aux déesses/dieux des couleurs

***

Croarc : croix-arc : crossbow

***

Reco JK : sombre machination globtopus

* Nommeur / façonneur (Patrick Rothfuss)

***

* faire un dev à la matrix, en spirale avec des caractères random qui disparaisent en formant des spirale puis les lettres changents. N&B, ubuntu font monopaced add greek, japan, cyrilic, symbol. Ajouter un grand des mots en ascci ignoré par la spirale.

***

Jeux à boire, dessous de bière, chaque dessous de bière est une classe, et on joue avec un dé

* Pour un tamagochi
** Est que la date UTC dépend de la locale ? https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date[Date], https://www.w3schools.com/jsref/jsref_now.asp[Now]
** https://home.openweathermap.org/api_keys[Météo]

***

== Rogue-Like social

* Météo
* Changement de saison tout les jours
* Alternance jour nuit

Un cercle de couleur pour :

* soi, nous
* l'interlocuteur
* il, eux

Parler avec des icônes, pour faciliter la traduction/internationalisation

Faire des énigmes

Que les PNJ ne soit pas présent tout le temps ou que la résolution nécessite une temporalité limité et ou des conditions de réussite, qu'il neige, fasse jour, ...

* Intimidation
* Persuasion
* Marché
* Vol

***

https://fr.wiktionary.org/wiki/Cat%C3%A9gorie:Termes_d%C3%A9suets_en_fran%C3%A7ais[Termes désuets en français]
https://fr.wikipedia.org/wiki/Liste_de_paradoxes[Liste de paradoxes]
https://www.editionstextuel.com/livre/la_panique_woke[la panique woke]

== Mantras

* Liberté ?
* Ce qui importe c'est quoi faire du temps qui nous ai impartis
* Sauvegarder la dignité des plus faibles
* Quiétude
* Lutter contre le pouvoir
* Justice
* S'exprimer
* Faire simple
* Faire une chose à la fois
* Ne rien faire
* Tu es déjà mort
* KISS

***

* dyade

https://www.geolocaux.com/vente/bureau/valence-26000/vente-bureau-58-m2-non-divisible-409924.html?utm_source=Lifull-connect&utm_medium=CPC&utm_campaign=premium

***

Association petite chaine JDR + studio (idée FibreTigre)

https://www.youtube.com/watch?v=le3GCzI7MW0

== Course

* https://laruchequiditoui.fr/fr/assemblies/5537/collections/373241/products/category/_all[laruchequiditoui]
* https://www.coco-et-lili.fr/[Coco et Lili]

== TEST JDR avec Rad

* D6 system
* Brigandyne
* SWADE
* Maze Rats
* PIOGRE
* FU

***

* asciitown sidescroll